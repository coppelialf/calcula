import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'

export default class Boutons extends React.Component{

    static defaultProps = {
        onPress: function(){},
        title: "",
        color: "", 
        backgroundColor: "",
        style:{}
    }

    render(){
        let bc = this.props.backgroundColor

        return(
            <TouchableOpacity onPress={this.props.onPress} 
                style={[styles.Boutons, {backgroundColor:bc}, {...this.props.style}]}>
                <Text style={[styles.text, { color: this.props.color}]}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    Boutons:{ 
        alignItems: "center", 
        justifyContent: "center", 
        width:80, 
        height:80, 
        margin: 5,
        borderRadius:5,
        borderColor: "#D3D3D3",
        borderWidth: 2,
        borderStyle:"inset"
    },

    text:{ 
        fontSize:40, 
        fontWeight:"500"}
});