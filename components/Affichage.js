import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default class Affichage extends React.Component{

    static defaultProps = {
        affichage:"",
    }

    render() {
        return (
            <View style={styles.screen}>
                <Text style={styles.affichage}>{this.props.affichage}</Text>
            </View>
        )
    }
}

const styles= StyleSheet.create({
    screen:{
        padding:30,
        margin: 30,
        backgroundColor: "#D3D3D3",
        borderRadius:20,
        borderColor: "white",
        borderWidth: 2,
        borderStyle:"inset"
    },
    
    affichage:{
        fontSize:70,
        color: "black",
        textAlign:"right"
    },
})