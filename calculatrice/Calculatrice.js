require("./../librairieMath/swisscalc.lib.format.js")
require("./../librairieMath/swisscalc.lib.operator.js")
require("./../librairieMath/swisscalc.lib.operatorCache.js")
require("./../librairieMath/swisscalc.lib.shuntingYard.js")
require("./../librairieMath/swisscalc.calc.calculator.js")
require("./../librairieMath/swisscalc.display.memoryDisplay")
require("./../librairieMath/swisscalc.display.numericDisplay")

import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Boutons, Affichage } from './../components'


export default class Calculatrice extends React.Component{

    constructor(props){
        super(props)
        this.state = {affichage: "0"}
        // initialisation de la calculatrice avec librairie Swisscalc
        this.swissCache = global.swisscalc.lib.operatorCache
        this.calculatrice = new global.swisscalc.calc.calculator()
    }

// Vue physique de la calculatrice 
    render(){
        return(
            <View style={styles.Wrapper}>
                <View style={styles.Screen}>
                    <Affichage affichage={this.state.affichage}/>
                </View>

                <View style={styles.ButtonBox}>
                    <View style={styles.button}>
                        <Boutons onPress={this.clear} title="C" color="#FFE279" backgroundColor="#304F8C"/>
                        <Boutons onPress={this.clear} title="DEL" color="#FFE279" backgroundColor="#304F8C"/>
                        <Boutons onPress={() => {this.pourcentage(this.swissCache.PercentOperator)}} title="%" color="#304F8C" backgroundColor="#FFE279"/>
                        <Boutons onPress={() => {this.operation(this.swissCache.DivisionOperator)}} title="÷" color="#304F8C" backgroundColor="#FFE279"/>
                    </View>

                    <View style={styles.button}>
                        <Boutons onPress={() => {this.chiffre("7")}} title="7" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.chiffre("8")}} title="8" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.chiffre("9")}} title="9" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.operation(this.swissCache.MultiplicationOperator)}} title="x" color="#304F8C" backgroundColor="#FFE279"/>
                    </View>

                    <View style={styles.button}>
                        <Boutons onPress={() => {this.chiffre("4")}} title="4" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.chiffre("5")}} title="5" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.chiffre("6")}} title="6" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.operation(this.swissCache.SubtractionOperator)}} title="-" color="#304F8C" backgroundColor="#FFE279"/>
                    </View>

                    <View style={styles.button}>
                        <Boutons onPress={() => {this.chiffre("1")}} title="1" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.chiffre("2")}} title="2" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.chiffre("3")}} title="3" color="#FFE279" backgroundColor="#638BBF"/>
                        <Boutons onPress={() => {this.operation(this.swissCache.AdditionOperator)}} title="+" color="#304F8C" backgroundColor="#FFE279"/>
                    </View>

                    <View style={styles.button}>
                        <Boutons onPress={() => {this.chiffre("0")}} title="0" color="#FFE279" backgroundColor="#638BBF" />
                        <Boutons onPress={() => {this.chiffre(".")}} title="." color="#FFE279" backgroundColor="#638BBF" />
                        <Boutons onPress={this.resultat} title="=" color="#FFE279" backgroundColor="#304F8C" />
                        <Boutons onPress={this.plusOuMoins} title="+/-" color="#304F8C" backgroundColor="#FFE279"/>
                    </View>

                </View>
                

            </View>
    );
}   

// Fonctions pour les boutons pressés
    chiffre = (digit) =>{
        this.calculatrice.addDigit(digit)
        this.setState({ affichage: this.calculatrice.getMainDisplay() 
        })
    }

    clear = () => {
        this.calculatrice.clear()
        this.setState({ affichage: this.calculatrice.getMainDisplay() 
        })
    }

    operation = (operator) => {
        this.calculatrice.addBinaryOperator(operator)
        this.setState({ affichage: this.calculatrice.getMainDisplay() 
        })
    }

    plusOuMoins = () => {
        this.calculatrice.negate()
        this.setState({ affichage: this.calculatrice.getMainDisplay() 
        })       
    }

    pourcentage = (operator) => {
        this.calculatrice.addUnaryOperator(operator)
        this.setState({ affichage: this.calculatrice.getMainDisplay() 
        })      
    }

    resultat = () => {
        this.calculatrice.equalsPressed()
        this.setState({ affichage: this.calculatrice.getMainDisplay() 
        })       
    }
}

// Style général de la calculatrice
const styles = StyleSheet.create({
    Wrapper: {
        flex:1, 
        backgroundColor:"black"
    },
    Screen: {
        flex:1, 
        justifyContent:"flex-end",
    },
    ButtonBox:{
        paddingBottom: 30,
        paddingLeft: 20,
        paddingRight: 20
    },
    button: {
        flexDirection:"row", 
        justifyContent:"space-between"}

})