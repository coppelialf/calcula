// Ressource utilisée pour le travail : https://www.youtube.com/watch?v=kye4zEwDxgU

import React from 'react'
import Calculatrice from './calculatrice/Calculatrice'

export default class App extends React.Component{
  render(){
    return <Calculatrice/>;
  }
}